var levels = [
   {"name": "w1-1.mp4", "timings": [21.07, 31.13, 39.50, 47.22]},
   {"name": "w1-2.mp4", "timings": [20.03, 27.90, 36.20, 45.50]},
   {"name": "w1-3.mp4", "timings": [23.33, 32.70, 41.27, 49.33]},
   {"name": "w2-1.mp4", "timings": [23.70, 32.27, 40.77, 50.43]},
   {"name": "w2-2.mp4", "timings": [27.53, 37.00, 45.60, 53.57]},
   {"name": "w2-3.mp4", "timings": [25.73, 33.63, 41.93, 51.30]},
   {"name": "w3-1.mp4", "timings": [24.43, 32.47, 40.73, 50.57]},
   {"name": "w3-2.mp4", "timings": [23.43, 32.80, 41.13, 49.10]},
   {"name": "w3-3.mp4", "timings": [38.87, 47.13, 56.93, 65.43]},
   {"name": "w4-1.mp4", "timings": [38.70, 46.50, 57.00, 65.40]},
   {"name": "w4-2.mp4", "timings": [31.87, 41.53, 50.13, 57.97]},
   {"name": "w4-3.mp4", "timings": [34.80, 42.97, 51.47, 61.10]},
   {"name": "w5-1.mp4", "timings": [41.67, 50.47, 59.03, 66.87]},
   {"name": "w5-2.mp4", "timings": [40.80, 49.23, 57.70, 67.10]},
   {"name": "w5-3.mp4", "timings": [61.67, 71.20, 79.90, 87.94]}
];

var ntrain = 2; // Nombre de niveaux d'entraînement par monde

(function () {
	var container = document.getElementById("container");
	var video = document.getElementById("player");
	var overlay = document.getElementById("overlay");
	var startbtn = document.getElementById("startbtn");
	var nextbtn = document.getElementById("nextbtn");
	var form = document.getElementById("formulaire");
	var startexpbtn = document.getElementById("startexp");
	
	var choices = [];
	var step = 0;
	var nextStop = 0;
	var level = 0;
	var timestamp;
	var records = [];
	var lstops;
    var formvals;
    var token = document.getElementById("token").value;
    var committed = {};
    var vidloadint;
	
	initWhole();
	
	function initWhole () {
		container.style.display = "none";
		form.onsubmit = submitForm;
        
        if (localStorage.token != token) {
            clearStorage();
        } else {
            initGame(true);
        }
	}

    // Storage management
    function loadStorage () {
        committed = JSON.parse(localStorage.committed);
        formvals = JSON.parse(localStorage.formvals);
        records = JSON.parse(localStorage.records);
        level = records.length;
    }
    function clearStorage () {
        localStorage.removeItem("committed");
        localStorage.removeItem("formvals");
        localStorage.removeItem("token");
        localStorage.removeItem("records");
    }
    function updateStorage () {
        localStorage.setItem("token", token);
        localStorage.setItem("committed", JSON.stringify(committed));
        localStorage.setItem("formvals", JSON.stringify(formvals));
        localStorage.setItem("records", JSON.stringify(records));
    }

    // Enregistrement du formulaire de début
	function submitForm () {
		var valide = true;
		var formels = form.elements;
		var values = {};
		for (var i=0; i<formels.length; i++) {
			var el = formels[i];
			if (!el.name || el.name.charAt(0) != "q") continue;
			var validel = el.checkValidity();
			if (!validel) valide = false;
            var lname = el.name.slice(2);
			if(el.type != "radio") values[lname] = el.value;
			else if(el.checked) values[lname] = el.value;
		}
        values["ntrain"] = ntrain;
        values["screen"] = window.screen.width + "x" + window.screen.height;
        formvals = values;
		console.log(values);
        commitResults();
		if (!valide) alert("Le formulaire est incomplet");
		else initGame();
		return false;
	}

    // Début du jeu
	function initGame (reload) {
		container.style.display = "";
		form.style.display = "none";
		startbtn.onclick = nextStep;
		nextbtn.onclick = nextStep;
		startexpbtn.onclick = nextStep;
		
		for (var i=0; i<3; i++) {
			choices[i] = document.getElementById("choice-"+i);
			choices[i].onclick = makeChoice;
		}
		step = -2;
        if (reload) {
            overlay.className = "reload";
            $("#btn-reload").on("click", function () {
                loadStorage();
                nextStep();
            });
        } else {
            nextStep();
        }
	}

    // Callback boutons
	function makeChoice() {
		nextStep(this.id.split('-')[1]*1);
	}

    // Contrôle de la vidéo
	function videoControl () {
		if (step <= 0) return;
		if (video.currentTime >= nextStop-0.25) {
			video.pause();
			video.currentTime = nextStop;
            nextStop = +Infinity;
			nextStep();
		}
	}
	function videoLoading () {
		if (video.buffered.length > 0 && lstops.length > 0) {
            startexpbtn.style.backgroundSize = (video.buffered.end(0)/lstops[0] * 100) + "% 100%";
            if(video.buffered.end(0) >= lstops[0]) {
			    startexpbtn.removeAttribute("disabled");
			    startexpbtn.innerText = "Entrer dans le niveau";
			    //video.onprogress = undefined;
                clearInterval(vidloadint);
            }
		}
	}

    // Le jeu, étape par étape
	function nextStep (param) {
		switch(step) {
			case -2:
				// Préparation d'un niveau
				step = -1;
				overlay.className = "intro";
				if (level >= levels.length) {
                    exitFullScreen();
					overlay.querySelector(".leveltitle").innerText = "Fin de l'expérience";
					overlay.querySelector(".levelsubtitle").innerText = "Merci d'avoir participé !";
					startexpbtn.style.display = "none";
					video.style.display = "none";
					return;
				}
                requestFullScreen(document.body);
				overlay.querySelector(".leveltitle").innerText = (level < ntrain) ? "Niveau d'entraînement" : "Niveau de test";
				overlay.querySelector(".levelsubtitle").innerText = "Monde "+Math.floor(level/3+1)+"/5, niveau "+(level%3+1) + "/3";
				startexpbtn.innerText = "Chargement...";
				startexpbtn.setAttribute("disabled", "");
				lstops = levels[level].timings;
				video.src = "videos/"+levels[level].name;
				video.ontimeupdate = videoControl;
				//video.onprogress = videoLoading;
				video.load();
                video.play();
                //setTimeout(function(){video.pause();}, 10);
				vidloadint = setInterval(videoLoading, 100);
				return;
			case -1:
				// Préparation de l'expérience
				overlay.className = "start";
				video.currentTime = 0;
				video.pause();
				step = 0;
				return;
			case 0:
				// Démarrage de l'expérience
				overlay.className = "hidden";
				nextStop = lstops[0]-0.10;
				video.currentTime = 0;
				video.play();
				step = 1;
				return;
			case 1:
				// Questions
				overlay.className = "choice";
				step = 2;
				timestamp = new Date().getTime();
				return;
			case 2:
				// Question répondue
				overlay.className = "hidden";
				records.push([param, (new Date().getTime() - timestamp) / 1000]);
				console.log(records[records.length-1]);
				video.currentTime = lstops[param]+0.20;
				nextStop = lstops[param+1]-0.10;
				video.play();
				step = 3;
                commitResults();
				return;
			case 3:
				// Fin de la vidéo
				overlay.className = "end";
				step = 4;
				return;
			case 4:
                // Début de la suivante
				step = -2;
				level++;
				nextStep();
				return;
		}
	}

    // AJAX pour enregistrement des résultats
    function commitResults () {
        // MàJ des enregistrements locaux
        updateStorage();
        
        var vars = {"token": token};
        var committing = [];
        var clevels = [];
        
        if (!committed["form"]) {
            vars = $.extend(vars, formvals);
            committing.push("form");
        }
        for (var k=0; k<records.length; k++) {
            if (!committed["v"+k]) {
                committing.push("v"+k);
                clevels.push(k+";"+records[k].join(";"));
            }
        }
        if (clevels.length > 0) vars["levels"] = clevels;
        
        // Fin de l'expérience : tous les niveaux sont passés
        if (committed.length + committing.length == levels.length + 1)
            vars["end"] = true;
        
        $.post("backend.php", vars, function(data) {
            if(data.success != true) alert("Erreur : "+data);
            else {
                for (var k=0; k<committing.length; k++) {
                    committed[committing[k]] = true;
                }
                updateStorage();
            }
        });
    }

    function requestFullScreen(element) {
        var fun = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;

        if (fun) {
            fun.call(element);
        } else if (typeof window.ActiveXObject !== "undefined") {
            var fscript = new ActiveXObject("WScript.Shell");
            if (fscript !== null) {
                fscript.SendKeys("{F11}");
            }
        }
    }
    
    function exitFullScreen() {
        var fun = document.exitFullScreen || document.webkitExitFullScreen || document.mozCancelFullScreen || document.msExitFullScreen;
        if (fun) {
            fun.call(document);
        } else if (typeof window.ActiveXObject !== "undefined") {
            var fscript = new ActiveXObject("WScript.Shell");
            if (fscript !== null) {
                fscript.SendKeys("{Esc}");
            }
        }
    }
})();
