﻿<?php

require("init.php");
if (!isset($_SESSION["token"]))
    header("Location: index.php");

?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Expérience de navigation passive</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <meta content="width=device-width, initial-scale=1.0; maximum-scale=1.0; minimum-scale=1.0;" name="viewport">
  </head>
  
  <body>
    <form id="formulaire" method="POST">
      <input type="hidden" id="token" name="token" value="<?php echo $_SESSION["token"] ?>" />
      <h1>Expérience d'orientation passive</h1>
      <p>Bienvenue dans l'expérience de jeu "Sea Hero Quest" en modalité passive. Avant de commencer, merci de répondre à ces quelques questions.</p>
        <ul class="questions">
          <li>
            <label for="q-sexe">Quel est votre sexe ?</label>
            <ul class="radios">
              <li><input type="radio" name="q-sexe" value="homme" id="q-sexe-homme" required><label for="q-sexe-homme">Homme</label></li>
              <li><input type="radio" name="q-sexe" value="femme" id="q-sexe-femme"><label for="q-sexe-femme">Femme</label></li>
            </ul>
          </li>
          <li>
            <label for="q-age">Quel âge avez-vous ?</label>
            <input type="number" name="q-age" min="16" max="100" value="" id="q-age" required/>
          </li>
          <li>
            <label for="q-pays">Dans quel pays vivez-vous ?</label>
            <input type="text" name="q-pays" value="France" id="q-pays" required/>
          </li>
          <li>
            <label for="q-lat">Avec quelle main écrivez-vous ?</label>
            <ul class="radios">
              <li><input type="radio" name="q-lat" value="gauche" id="q-lat-gauche" required><label for="q-lat-gauche">La gauche</label></li>
              <li><input type="radio" name="q-lat" value="droite" id="q-lat-droite"><label for="q-lat-droite">La droite</label></li>
            </ul>
          </li>
          <li>
            <label for="q-etudes">Quel est votre niveau d'études ?</label>
            <ul class="radios long">
              <li><input type="radio" name="q-etudes" value="aucune" id="q-etudes-aucune"><label for="q-etudes-aucune">Aucune éducation formelle</label></li>
              <li><input type="radio" name="q-etudes" value="lycee" id="q-etudes-lycee"><label for="q-etudes-lycee">Lycée</label></li>
              <li><input type="radio" name="q-etudes" value="univ" id="q-etudes-univ"><label for="q-etudes-univ">Université</label></li>
              <li><input type="radio" name="q-etudes" value="sup" id="q-etudes-sup"><label for="q-etudes-sup">Etudes supérieures</label></li>
            </ul>
          </li>
          <li>
            <label for="q-trajet">Quel est votre temps de trajet quotidien ?</label>
            <ul class="radios long">
              <li><input type="radio" name="q-trajet" value="court" id="q-trajet-court" required><label for="q-trajet-court">Moins de 30 minutes</label></li>
              <li><input type="radio" name="q-trajet" value="moyen" id="q-trajet-moyen"><label for="q-trajet-moyen">30 minutes - 1 heure</label></li>
              <li><input type="radio" name="q-trajet" value="long" id="q-trajet-long"><label for="q-trajet-long">1 heure ou plus</label></li>
            </ul>
          </li>
          <li>
            <label for="q-orientation">Comment caractériseriez-vous vos capacités d'orientation ?</label>
            <ul class="radios">
              <li><input type="radio" name="q-orientation" value="a" id="q-orientation-a" required><label for="q-orientation-a">Excellentes</label></li>
              <li><input type="radio" name="q-orientation" value="b" id="q-orientation-b"><label for="q-orientation-b">Bonnes</label></li>
              <li><input type="radio" name="q-orientation" value="d" id="q-orientation-d"><label for="q-orientation-d">Mauvaises</label></li>
              <li><input type="radio" name="q-orientation" value="e" id="q-orientation-e"><label for="q-orientation-e">Très mauvaises</label></li>
            </ul>
          </li>
          <li>
            <label for="q-sommeil">Combien d'heures dormez-vous par nuit en moyenne ?</label>
            <input type="number" name="q-sommeil" min="2" max="16" value="" id="q-sommeil" required/>
          </li>
          <li>
            <label for="q-env">Dans quel type d'environnement avez-vous grandi ?</label>
            <ul class="radios">
              <li><input type="radio" name="q-env" value="ville" id="q-env-ville" required><label for="q-env-ville">Ville</label></li>
              <li><input type="radio" name="q-env" value="banlieue" id="q-env-banlieue"><label for="q-env-banlieue">En banlieue</label></li>
              <li><input type="radio" name="q-env" value="campagne" id="q-env-campagne"><label for="q-env-campagne">Campagne</label></li>
              <li><input type="radio" name="q-env" value="melange" id="q-env-melange"><label for="q-env-melange">Mélange</label></li>
            </ul>
          </li>
          <li>
            <label for="q-activ">Quel a été votre niveau d'activité au cours des 6 derniers mois ?</label>
            <ul class="radios long">
              <li><input type="radio" name="q-activ" value="1h" id="q-activ-1h" required><label for="q-activ-1h">Moins d'1 heure d'activité par semaine</label></li>
              <li><input type="radio" name="q-activ" value="2h" id="q-activ-2h"><label for="q-activ-2h">1 à 2 heures par semaine</label></li>
              <li><input type="radio" name="q-activ" value="3h" id="q-activ-3h"><label for="q-activ-3h">2 à 3 heures par semaine</label></li>
              <li><input type="radio" name="q-activ" value="4h" id="q-activ-4h"><label for="q-activ-4h">3 heures ou plus par semaine</label></li>
              <li><input type="radio" name="q-activ" value="pro" id="q-activ-pro"><label for="q-activ-pro">Pratique professionnelle</label></li>
            </ul>
          </li>
          <li>
              <label for="q-jeu">Avez-vous déjà joué au jeu "Sea Hero Quest" (plus de 3 niveaux) ?</label>
              <ul class="radios">
                  <li><input type="radio" name="q-jeu" value="non" id="q-jeu-non" required><label for="q-jeu-non">Non</label></li>
                  <li><input type="radio" name="q-jeu" value="oui" id="q-jeu-oui" required><label for="q-jeu-oui">Oui</label></li>
              </ul>
          </li>
        </ul>
        <input type="submit" value="Valider" />
    </form>
    <div id="container">
      <div id="video">
        <video id="player" src="" width="100%" height="100%" playsinline>
      </div>
      <div id="overlay" class="start">
        <div class="splasher"><div id="splash">
          <h1 class="leveltitle"></h1>
          <p class="levelsubtitle"></p>
          <button id="startexp">Chargement...</button>
        </div></div>
        <div class="starter"><a href="javascript:void(0)" id="startbtn" class="lowbtn">Commencer l'expérience</a></div>
        <div class="nexter"><a href="javascript:void(0)" id="nextbtn" class="lowbtn">Suite</a></div>
        <ul class="options">
            <li><a href="javascript:void(0);" id="choice-0">A</a></li>
            <li><a href="javascript:void(0);" id="choice-1">B</a></li>
            <li><a href="javascript:void(0);" id="choice-2">C</a></li>
        </ul>
        <div class="reloader">
            <form action="backend.php" method="POST" id="reload"> 
                <h2>Continuer l'expérience</h2>
                <p>Une expérience semble être en cours, voulez-vous la reprendre là où vous l'aviez laissée ?</p>
                <input type="hidden" name="token" value="<?php echo $_SESSION["token"]; ?>" /><input type="hidden" name="redirect" value="true" />
                <p align="center"><button id="btn-reload" onclick="return false;" >Continuer</button> <input type="submit" name="end" value="Nouvelle expérience" /></p>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="controller.js"></script>
  </body>
</html>
