<?php
require("../init.php");

function initDB ($db, $table) {
    $create_sql = "CREATE TABLE $table (
      token VARCHAR(32) PRIMARY KEY,
      date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      sexe VARCHAR(1) NOT NULL,
      age SMALLINT NOT NULL,
      pays VARCHAR(64) NOT NULL,
      lat VARCHAR(1) NOT NULL,
      etudes VARCHAR(1),
      trajet VARCHAR(1) NOT NULL,
      orientation VARCHAR(1) NOT NULL,
      sommeil SMALLINT NOT NULL,
      env VARCHAR(1) NOT NULL,
      activ VARCHAR(1) NOT NULL,
      ntrain SMALLINT NOT NULL";

    for($i=0; $i<15; $i++) {
        $create_sql .= ",
      q${i}_a SMALLINT,
      q${i}_t FLOAT";
    }

    $create_sql .= "
    );";

    $query = $db -> prepare($create_sql);
    $query -> execute();
    $error = $query -> errorInfo();
    $query -> closeCursor();
    return $error;
}

$error = initDB($db, $table);

if ($error[0] != '00000') {
    echo "Erreur :\n";
    print_r($error);
} else {
    echo "OK";
}

function alterDB ($db, $table) {
    $alter_sql = "ALTER TABLE $table 
      ADD COLUMN jeu VARCHAR(1) AFTER activ,
      ADD COLUMN screen VARCHAR(15) AFTER jeu,
      ADD COLUMN ua VARCHAR(512) AFTER screen";

    $query = $db -> prepare($alter_sql);
    $query -> execute();
    $error = $query -> errorInfo();
    $query -> closeCursor();
    return $error;
}

$error = alterDB($db, $table);

if ($error[0] != '00000') {
    echo "Erreur :\n";
    print_r($error);
} else {
    echo "OK";
}

?>
