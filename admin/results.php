<?php
require("../init.php");

$query = $db->prepare("SELECT * FROM shq_passif ORDER BY date DESC");
$query -> execute();
$results = $query->fetchAll(PDO::FETCH_ASSOC);
$query -> closeCursor();
$gans = array(0, 2, 0,
              2, 0, 2,
              2, 0, 1,
              1, 0, 2,
              0, 2, 0);

?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Résultats</title>
        <style type="text/css">
         body {
             font: 14px "sans-serif";
         }
         .table {
             display: table;
             cellspacing: 3px;
         }
         .row {
             display: table-row;
         }
         .row span:not(.comma) {
             text-align: center;
             display: table-cell;
             border-bottom: 3px solid transparent;
             background: #eee;
             background-clip: padding-box;
             padding: 3px;
         }
         .row:nth-child(2n) span:not(.comma) {
             background-color: #f7f7f7;
         }
         .row span.c-good {
             color: #0f0;
         }
         .row span.c-bad {
             color: #f00;
         }
         .row span.comma {
             display: table-cell;
             opacity: 0;
             width: 0;
             overflow: hidden;
         }
         .val-token {
             max-width: 100px;
             overflow: hidden;
             text-overflow: ellipsis;
         }
         .first-row {
             font-weight: bold;
         }
        </style>
    </head>

    <body>
        <div class="table">
            <?php
            foreach ($results as $key => $values) {
                if ($key == 0) {
                    echo "<p class=\"row first-row\"><span>Clés</span>";
                    foreach ($values as $kname => $val) {
                        echo "<span class=\"comma\">; </span><span>$kname</span>";
                    }
                    echo "</p>\n";
                }
                echo "<p class=\"row\"><span>$key</span>";
                $n = 0;
                foreach ($values as $kname => $val) {
                    $classes = "";
                    if (strchr($val, ";")) $val = "\"$val\"";
                    if ($kname[0] == 'q' && $kname[strlen($kname)-1] == 'a') {
                        $classes = ($gans[$n] == $val ? "c-good" : "c-bad");
                        $n ++;
                    }
                    echo "<span class=\"comma\">; </span><span class=\"val-$kname $classes\">$val</span>";
                }
            }
            ?>
        </div>
    </body>
</html>
