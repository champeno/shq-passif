<?php

// Connexion à la base de données
$db = new PDO("mysql:host=localhost;dbname=shq_passif", "shq", "toto");
$table = 'shq_passif';

session_start();

function generateRandomString($length = 32) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateUniqueID ($db, $table) {
    // Créateur de tokens
    $query = $db -> prepare("SELECT COUNT(*) FROM $table WHERE token = :token");
    while (true) {
        $id = generateRandomString();
        // Teste si le token existe
        $query -> execute(array(':token' => $id));
        $exists = $query -> fetch();
        $query -> closeCursor();

        // Si c'est bon, on le garde
        if ($exists[0] == 0) {
            return $id;
        }
    }
}

?>
