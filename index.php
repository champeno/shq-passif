<?php

require("init.php");
if(!isset($_SESSION["token"]))
    $_SESSION["token"] = generateUniqueID($db, $table);

?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Expérience de navigation passive</title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <meta content="width=device-width, initial-scale=1.0; maximum-scale=1.0; minimum-scale=1.0;" name="viewport">
    </head>
    
    <body>
        <div class="content">
            <h1>Expérience de navigation passive</h1>

            <p>La perte du sens de l'orientation est un des premiers symptômes de la démence. Pourtant, il n'est pas toujours possible de le diagnostiquer, car les capacités humaines de navigation sont mal connues, et mal évaluées.</p>
            <p><a href="http://www.seaheroquest.com/site/fr/" target="_blank">Sea Hero Quest</a> est un jeu vidéo qui vise à collecter des données à grande échelle sur le sujet. Avec plus de 3 millions de joueurs, une quantité importante d'informations a pu ainsi être fournie à la recherche.</p>
            <p>Le jeu mobile propose des défis spatiaux où le joueur contrôle les déplacements. L'expérience qui vous est proposée ici vise à tester les mêmes tâches en modalité passive, c'est-à-dire quand vous ne contrôlez pas les déplacements.</p>

            <img src="videos/shq.jpg" class="exemple" />
            <h3>Description de l'expérience</h3>
            <p>Après un questionnaire sur vos capacités d'orientation et des facteurs pouvant les influencer - identique à celui utilisé dans <i>Sea Hero Quest</i> -, il vous sera demandé de jouer successivement à tous les niveaux "fusée de détresse" du jeu, en modalité passive.</p>
            <p>En pratique, vous allez visionner, à chaque fois, une vidéo d'un bateau partant d'une balise (en forme de panier de basket), jusqu'à un lance-fusée. Une fois arrivé à destination, vous allez devoir choisir, entre trois options possibles, la direction dans laquelle se trouve la balise (i.e., votre point de départ).</p>
            
            <h3>Données collectées</h3>
            <p>Les informations suivantes seront conservées :</p>
            <ul>
                <li>Vos réponses au formulaire de début de jeu ;</li>
                <li>La résolution de votre écran et le type de terminal (téléphone, ordinateur...) ;</li>
                <li>Vos réponses à chaque niveau et votre temps de réponse.</li>
            </ul>
            <p>La collecte est anonyme, aucune information permettant de vous identifier n'est conservée. Seul⋅e⋅s les chercheur⋅se⋅s impliqué⋅e⋅s dans cette étude auront accès à ces données.</p>
            <p>Vous devez avoir 18 ans ou plus pour participer.</p>

            <h3>Détails pratiques</h3>
            <p>Si un problème a lieu pendant l'expérience, ou qu'un niveau peine à se charger, il est possible d'actualiser la page : l'expérience reprendra là où vous l'aurez laissée.</p>
            <p>L'expérience totale dure une quinzaine de minutes (15 niveaux de moins d'une minute). Elle demande le téléchargement de 80 Mo de vidéo (sans son).</p>
            <p>L'interface fonctionne sous les versions récentes de Firefox, Chrome ou Safari. Les autres navigateurs n'ont pas été testés.</p>

            <h3>Informations de contact</h3>
            <p>Cette expérience est menée par Robin Champenois et Roberto Casati (École normale supérieure, Paris). Si vous avez des questions concernant cette étude, vous pouvez les adresser à robin [point] champenois [arobase] ens [point] fr</p>

            <p>En cliquant sur le lien suivant, vous déclarez accepter les conditions ci-dessus :</p>
            <h3 align="center"><a href="experience.php"><button>Aller à l'expérience</button></a></h3>
        </div>
    </body>
</html>
