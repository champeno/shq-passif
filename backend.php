<?php
require("init.php");

function createEntry ($db, $table, $token) {
    // Création de la ligne dans la base de données
    
    $query = $db -> prepare("INSERT INTO $table (
        token,  sexe,  age,  pays,  lat,  etudes,  trajet,  orientation, 
        sommeil,  env,  activ, jeu, ua, screen, ntrain
    ) VALUES (
       :token, :sexe, :age, :pays, :lat, :etudes, :trajet, :orientation,
       :sommeil, :env, :activ, :jeu, :ua, :screen, :ntrain
    )");
    
    $sexe = substr($_POST["sexe"], 0, 1);
    $age = intval($_POST["age"] * 1);
    $pays = substr($_POST["pays"], 0, 64);
    $lat = substr($_POST["lat"], 0, 1);
    $etudes = (isset($_POST["etudes"])
             ? substr($_POST["etudes"], 0, 1)
             : NULL);
    $trajet = substr($_POST["trajet"], 0, 1);
    $orientation = substr($_POST["orientation"], 0, 1);
    $sommeil = intval($_POST["sommeil"] * 1);
    $env = substr($_POST["env"], 0, 1);
    $activ = substr($_POST["activ"], 0, 1);
    $jeu = substr($_POST["jeu"], 0, 1);
    $ua = $_SERVER['HTTP_USER_AGENT'];
    $screen = $_POST["screen"];
    $ntrain = intval($_POST["ntrain"] * 1);
    
    $query -> execute(
        array(
            ":token" => $token, ":sexe" => $sexe, ":age" => $age,
            ":pays" => $pays, ":lat" => $lat, ":etudes" => $etudes,
            ":trajet" => $trajet, ":orientation" => $orientation,
            ":sommeil" => $sommeil, ":env" => $env, ":activ" => $activ,
            ":jeu" => $jeu, ":screen" => $screen, ":ua" => $ua,
            ":ntrain" => $ntrain
        )
    );
    
    $error = $query -> errorInfo();
    $query -> closeCursor();

    if ($error[0] == "23000") {
        // Duplicate entry : on ignore
        $error[0] = "00000";
    }
    
    return $error;
}

function commitResults ($db, $table, $levels, $token) {
    // Enregistre les résultats d'un ensemble de niveaux
    $vars = array(":token" => $token);
    $updates = array();
    foreach($levels as $key => $val) {
        $parts = explode(";", $val);
        $level = intval($parts[0]);
        $answer = $parts[1]*1;
        $time = $parts[2]*1;
        array_push($updates, "q${level}_a = :qa$key", "q${level}_t = :qt$key");
        $vars[":qa$key"] = $answer;
        $vars[":qt$key"] = $time;
    }
    $req = "UPDATE $table SET ".join($updates, ", ")." WHERE token = :token";
    $query = $db -> prepare($req);
    $query -> execute($vars);
    $error = $query -> errorInfo();
    $query -> closeCursor();
    return $error;
}

$result = array();

// Lecture du token
if (!isset($_POST["token"])) {
    die("Accès interdit");
}
if (!isset($_SESSION["token"])) {
    die("Erreur : session invalide");
}

$token = $_SESSION["token"];
if ($_POST["token"] != $token) {
    die("Erreur : session incohérente");
}

$result["success"] = true;
$result["error"] = array();

if (isset($_POST["sexe"])) {
    // Formulaire pré-expérience
    $error = createEntry($db, $table, $token, $_POST);
    if ($error[0] != '00000') {
        $result["success"] = false;
        array_push($result["error"], $error);
    }
}

if (isset($_POST["levels"])) {
    // Résultat d'un ensemble de niveaux
    $error = commitResults($db, $table, $_POST["levels"], $token);
    if ($error[0] != '00000') {
        $result["success"] = false;
        array_push($result["error"], $error);
    }
}

// Fin de l'expérience
if (isset($_POST["end"])) {
    unset($_SESSION["token"]);
    if (isset($_POST["redirect"])) {
        header("Location: index.php");
        exit;
    }
}

// Résultat
header("Content-type: text/json; charset=utf-8;");
echo json_encode($result);
?>
